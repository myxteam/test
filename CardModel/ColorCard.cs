﻿using System;

namespace Models
{
	public class ColorCard
	{
		public string Name { get; set; }
		public int  ColorValue { get; set; }
		public ColorCard(string Name, int ColorValue){
			this.Name = Name;
			this.ColorValue = ColorValue;
		}
	}
}

