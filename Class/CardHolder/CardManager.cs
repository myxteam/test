﻿using System;
using Models;
using System.Collections.Generic;

namespace CardPairing
{
	public class CardManager
	{
		public List<ColorCard> ColorCards { get; set;}
		Random rnd;
		public CardManager ()
		{
			rnd = new Random ();
		}
		public void addCards(int randomInt){
			if (this.ColorCards == null)
				this.ColorCards = new List<ColorCard> ();		
//			int randomInt = rnd.Next (1, 7);
//			Console.WriteLine (randomInt);
			switch (randomInt) {
			case 1:
				addDoubleCardColor ("RedCard", randomInt);
				break;
			case 2:
				addDoubleCardColor ("BlueCard", randomInt);
				break;
			case 3:
				addDoubleCardColor ("YellowCard", randomInt);
				break;
			case 4:
				addDoubleCardColor ("WhiteCard", randomInt);
				break;
			case 5:
				addDoubleCardColor ("OrangeCard", randomInt);
				break;
			case 6:
				addDoubleCardColor ("BlackCard", randomInt);
				break;
			default:
				addDoubleCardColor ("BlackCard", randomInt);
				break;
			}
		}
		public void addDoubleCardColor(string Name, int value){
			for (int i = 0; i < 2; i++) {
				this.ColorCards.Add (new ColorCard (Name, value));
			}
		}
		public void Shuffle(){
//			Random rnd = new Random ();
			for (int i = 0; i < this.ColorCards.Count; i++) {
				int rndIndex = rnd.Next (1, this.ColorCards.Count);
				Console.WriteLine (rndIndex);
				Swap (i, rndIndex);
			}
		}
		public void Swap(int Currentindex, int SwappingIndex){
			var temp = this.ColorCards [Currentindex];
			this.ColorCards [Currentindex] = this.ColorCards [SwappingIndex];
			this.ColorCards [SwappingIndex] = temp;
		}
		public void toGrid(ref ColorCard[,] colorCard){
			int cardIndex = 0;
			for (int x = 0; x < colorCard.GetLength (0); x++) {
				for (int y = 0; y < colorCard.GetLength (1); y++) {
					if (cardIndex <= this.ColorCards.Count) {
						Console.WriteLine (this.ColorCards [cardIndex].Name);
						colorCard [x, y] = this.ColorCards [cardIndex];
						cardIndex++;
					}
				}
			}
//			int x =0,y=0;
//			for (int i = 0; i < this.ColorCards.Count; i++) {
//				
//			}
		}
	}
}
	
