﻿ using System;
using Android.Widget;
using Android.Content;

namespace CustomImageViewer
{
	public class CustomImageView
	{
		ImageView imageView;
		int gridX =0, gridY = 0;

		public ImageView ImageView {
			get {
				return imageView;
			}
			set {
				imageView = value;
			}
		}

		public int GridX {
			get {
				return gridX;
			}
			set {
				gridX = value;
			}
		}

		public int GridY {
			get {
				return gridY;
			}
			set {
				gridY = value;
			}
		}
			
		public CustomImageView(){
		}
		public void onDispose(){
			if (imageView != null) {
				imageView.SetImageResource (0);
				imageView = null;
			}
		}
	}
}

