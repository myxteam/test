﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using CustomImageViewer;
using Models;
using DI.PoorMansContainer;
using Android.Content.PM;

namespace CardPairing
{
	[Activity (Label = "CardPairing", MainLauncher = true, Icon = "@drawable/icon",Theme = "@android:style/Theme.NoTitleBar.Fullscreen", ScreenOrientation = ScreenOrientation.SensorLandscape)]
	public class MainActivity : Activity
	{
		int count = 1;
		CardManager cardmanager;
		Container container;
		LinearLayout linearLayout;
		ColorCard[,] colorCardGrid;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			linearLayout = FindViewById<LinearLayout> (Resource.Id. linearLayout1);
			container = new Container ();
			cardmanager = new CardManager ();
			for (int i = 1; i < 7; i++) {
				cardmanager.addCards (i);
			}
			colorCardGrid = new ColorCard[4, 3];
			cardmanager.Shuffle ();
			cardmanager.toGrid (ref colorCardGrid);
			initGame (ref linearLayout, this, this.FragmentManager);
		}

		private int MarginValue(int count)
		{
			return 120/count;
		}
		private int pxFromDp(float dp, Context context)
		{
			return (int)(dp * context.Resources.DisplayMetrics.Density);//this.getContext().getResources().getDisplayMetrics().density;
		}


		public void initGame(ref LinearLayout linearLayout, Context context, FragmentManager frag)
		{

			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent, 1f);
			param.SetMargins (10, 10, 10, 10);
			int collumnsCount = MarginValue (3);
			collumnsCount = pxFromDp (collumnsCount, context);
			var par = new LinearLayout.LayoutParams (pxFromDp (80, context), pxFromDp (75, context));
			par.SetMargins (collumnsCount,0,collumnsCount,0);
			for (int x = 0; x < colorCardGrid.GetLength(0); x++)
			{
				LinearLayout linearLayoutextend = new LinearLayout(context);
				param.Gravity = Android.Views.GravityFlags.Center;
				linearLayoutextend.LayoutParameters = param;
				linearLayoutextend.SetGravity(Android.Views.GravityFlags.Center);
				for (int y = 0; y < colorCardGrid.GetLength(1); y++)
				{
					var customImageView = container.CreateType<CustomImageView>();
					customImageView.ImageView = new ImageView(context);
					customImageView.ImageView.LayoutParameters = par;
					switch (colorCardGrid[x,y].ColorValue)
					{
					case 1:
						customImageView.ImageView.SetBackgroundResource(Resource.Drawable.Red_card);
						break;
					case 2:
						customImageView.ImageView.SetBackgroundResource(Resource.Drawable.Blue_card);
						break;
					case 3:
						customImageView.ImageView.SetBackgroundResource(Resource.Drawable.Yellow_card);
						break;
					case 4:
						customImageView.ImageView.SetBackgroundResource(Resource.Drawable.White_card);
						break;
					case 5:
						customImageView.ImageView.SetBackgroundResource(Resource.Drawable.Orange_card);
						break;
					case 6:
						customImageView.ImageView.SetBackgroundResource(Resource.Drawable.Black_card);
						break;
					}
					customImageView.ImageView.Id = x;
					customImageView.GridX = x;
					customImageView.GridY = y;

					customImageView.ImageView.Click += async delegate(object sender, EventArgs e)
					{

					};
					linearLayoutextend.AddView(customImageView.ImageView);
				}
				linearLayout.AddView(linearLayoutextend);
			}
		}
	}
}


